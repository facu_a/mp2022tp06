

#Metodo para mostrar el menu - Facundo Alfaro
def menu():
    productos = {
        100: ['Harina',200.0,50],
        200: ['Vino',500.0,20],
        300: ['Queso',300.0,10],
        400: ["Cerveza",500.0,0]
    }

    flag=True

    while flag:

        print("1. Registrar productos")
        print("2. Mostrar productos")
        print("3. Mostrar productos en un rango[A-B] de stock")
        print("4. Buscar los productos con mayor precio")
        print("5. Sumar stock")
        print("6. Eliminar productos con stock 0")
        print("7. Salir")

        print("Ingrese su opcion")
        op=esNumero()

        if op==7:
            break
        
        ejecutarOpciones(op,productos)


#Funcion para validar si lo ingresado en consola es un numero 
def esNumero():
  while True:
    try:
      numero=int(input(""))
      break
    except :
      print("El valor debe ser NUMERICO")
  return numero 


#Metodo para ejecutar funciones de acuerdo a la opcion ingresada - Facundo Alfaro
def ejecutarOpciones(op, productos):

    if op == 1:
        registrarProductos(productos)
    elif op == 2:
        mostrarProductos(productos)
    elif op == 3:
        mostProductosRang(productos)
    elif op == 4:
         mostrarMasCaro(productos)
    elif op == 5:
        print()
    elif op == 6:
        print()


#Metodo el cual registra los productos  - Facundo Alfaro
def registrarProductos(productos):
    codigo = validarCodigo(productos)
    descripcion = ingresarDescripcion()
    precio = ingresarPrecio()
    stock = ingresarStock()

    productos[codigo] = [descripcion, precio, stock]
    
#Metodo para verificar si el codigo ingresado ya existe - Facundo Alfaro
def validarCodigo(productos):
    print("Ingrese el codigo")
    n=esNumero()
    if n in productos:
        print("El codigo ingresado ya existe")
    return n
 
#Metodo para ingresar la descripcion del producto - Facundo Alfaro
def ingresarDescripcion():
    print("Ingrese una descripcion del producto")
    des = input()
    return des


#Metodo el cual se ingresa el precio y verifica que no sea negativo - Facundo Alfaro
def ingresarPrecio():
    flag=True

    while flag:
        print("Ingrese el precio del producto")
        prc = float(input())
        if prc > 0:
            flag=False
            return prc
        print("El precio no debe ser negativo")

#Metodo que ingresa el stock y verifica que no sea negativo - Facundo Alfaro
def ingresarStock():
    flag=True

    while flag:
        print("Ingrese el stock del producto")
        prc = esNumero()
        if prc > 0:
            flag=False
            return prc
        print("El stock no debe ser negativo")




#Metodo que muestra los productos - Facundo Alfaro
def mostrarProductos(productos):
    for clave in productos:
        print(clave, productos[clave])




#Funcion para mostrar productos en un rango determinado de Stock-Noemi Luna
def mostProductosRang(productos):
  A=ingresoDeA()
  B=ingresoDeB(A)
  for codigo,K in productos.items():
    if A<productos[codigo][2]<B:
      print(codigo,K) 

#Funciones para validar el ingreso de A y B-Noemi Luna
def ingresoDeA():
  print("Ingrese el balor de A: ")
  A=esNumero()
  while A<0:
    print("--Error A no puede ser negativo ingrese nuebamente: ")
    A=esNumero()
  return A
def ingresoDeB(A):
  print("Ingrese B(debe ser mayor a A):")
  B=esNumero()
  while B<A:
    print("--Error B debe ser mayor a A ingrese nuebamente:")
    B=esNumero()
  return B

#Funcion para buscar el mayor precio-Noemi Luna
def buscarPrecioMayor(productos):
  maximo=0
  for codigo,K in productos.items():
    if maximo<productos[codigo][1]:
      maximo=productos[codigo][1]
  return maximo


#Funcion para mostrar produductos con mayor precio-Noemi Luna
def mostrarMasCaro(productos):
  maximoPrecio=buscarPrecioMayor(productos)
  for codigo,K in productos.items():
    if productos[codigo][1]==maximoPrecio:
      print(codigo,K)



def aumentarStock(productos):
    print("Ingrese el stock que desea aumentar")
    x = esNumero()
    for clave in productos:
        if productos[clave][2] <= x:
            print(clave, productos[clave])
    print("Elija que producto desea aumentar stock")
    cod = esNumero()
    print("Cuanto desea reponer?")
    stk=esNumero()
    productos[cod][2] += stk

def eliminarStock(productos):
    print("Desea eliminar objetos con stock 0?")
    for clave in productos.copy():
        if productos[clave][2] == 0:
            del productos[clave]








#Programa Principal 
menu()