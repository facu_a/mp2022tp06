

from math import prod
import os
from re import X

# Nahuen Zalasar

def menu():
    print('1) Registrar productos')
    print('2) Mostrar el listado de productos')
    print('3) Mostrar el listado de un determinado rango de valor de de stocks')
    #print('4) Buscar el precio mas alto y mostrar los productos que lo posean')
    print('4) Sumar stocks que sea menor a un valor Y ')
    print('5) Eliminar producto')
    print('6) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion


def ingresarCodigo():
    codigo = int(input('Ingresar un codigo: '))
    return codigo


def precioNoNegativo(): 
    precio = float(input('Ingrese un precio: '))
    while (precio < 0):
        print('El precio ingresado es negativo (debe ser positivo)')
        precio = float(input('Ingrese un precio: '))
    else:
        return precio

def stockNoNegativo():
    stock = int(input('Ingrese un stock: '))
    while (stock < 0):
        print('El stock debe ser positivo')
        stock = int(input('Ingrese un stock: '))
    else:
        return stock


def registrarProductos():
    continuar = 's'
    productos = {}
    while (continuar == 's'):
        codigo = ingresarCodigo()
        if codigo in productos:
            print('El producto ya existe')
        else:
            descripcion = input('Ingrese una descripcion: ')
            precio = precioNoNegativo()
            stock = stockNoNegativo()
            productos[codigo] = [descripcion, precio, stock]
            print('Agregado correctamente')
        continuar = input('¿Desea continar[s/n]: ')
    return productos

def mostrarProductos(productos1):
    print('---Listado de Productos---')
    for i,j in productos1.items():
        print(i,j)

def pedirValor():
    desde = int(input('Ingrese un valor: '))
    hasta = int(input('Ingrese un valor: '))
    return desde,hasta

def mostrarIntervalo(productos2):
    prod = dict()
    desde,hasta = pedirValor()
    for i, j in productos2.items():
        if desde <= j[2] <= hasta:
            prod[i] = j
    print('Productos que se encuentran en el intervalo ['+ str(desde) + '/' + str(hasta) +']: ',prod)

#def hallarPrecioAlto(productos3):
#    r = list(productos3.values())
#    mayor = r[0][1]
#    for i , j  in enumerate(r):
#        if r[i][1] > mayor:
#            mayor = r[i][1]
#    return mayor


#def productoAlto(prodN):
 #   mayor = hallarPrecioAlto(productos)
  #  prod = dict()
   # for i,j in prodN.items():
    #    if j[1] == mayor:
     #       prod[i] = j
   # print('El/los producto/os mas caros son: ',prod)

def ingresarN():
    n = int(input('Ingrese un valor: '))
    return n

def menorY(productos4):
    y = ingresarN()
    prodN = dict()
    for i,j in productos4.items():
        if j[2] < y:
            prodN[i] = j
    return prodN

def sumarStock(productos5):
    x = ingresarN()
    prodN2 = dict()
    for i,j in productos5.items():
        r = j[2] + x
        j[2] = r
        prodN2[i] = j
    print(prodN2)

def eliminarStock():
    prodE = productos
    prodA = dict()
    for i,j in prodE.items():
        if j[2] != 0:
            prodA[i] = j
    return prodA

#principal
os.system('cls')
opcion = 0
while (opcion != 7):
    opcion = menu()
    if opcion == 1:
        productos = registrarProductos()
    elif opcion == 2:
        mostrarProductos(productos)
    elif opcion == 3:
        mostrarIntervalo(productos)
    #elif opcion == 4:
        #productoAlto(productos)
    elif opcion == 4:
        r = menorY(productos)
        sumarStock(r)
    elif opcion == 5:    
        productos = eliminarStock()
        print(productos)
    elif opcion == 6:
        print('Fin del programa....')
